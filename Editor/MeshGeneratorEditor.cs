﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TerrainGridGenerator))]
public class MeshGeneratorEditor : Editor
{
    private GUIContent terrainSizeOptions;
    private bool diamondControls;
    private bool sinControls;

    public MeshGeneratorEditor()
    {
        terrainSizeOptions = new GUIContent("SIZE");
    }

    public override void OnInspectorGUI()
    {
        var meshGenerator = target as TerrainGridGenerator;
        
        EditorGUILayout.Space();

        meshGenerator.TerrainGenerationAlgorithm = (TerrainGridGenerator.GenerationAlgorithm)EditorGUILayout.EnumPopup("Algorithm", meshGenerator.TerrainGenerationAlgorithm);

        switch (meshGenerator.TerrainGenerationAlgorithm)
        {
            case TerrainGridGenerator.GenerationAlgorithm.SimpleRandom:
                diamondControls = false;
                sinControls = false;
                break;
            case TerrainGridGenerator.GenerationAlgorithm.Sinusoidal:
                diamondControls = false;
                sinControls = true;
                break;
            case TerrainGridGenerator.GenerationAlgorithm.DiamondSquare:
                diamondControls = true;
                sinControls = false;
                break;
            default:
                break;
        }

        EditorGUILayout.Space();

        using (var group = new EditorGUILayout.FadeGroupScope(System.Convert.ToSingle(diamondControls)))
        {
            if (diamondControls)
            {
                meshGenerator.TerrainSize = (int)(TerrainGridGenerator.Size)EditorGUILayout.EnumPopup("Size", (TerrainGridGenerator.Size)meshGenerator.TerrainSize);
                meshGenerator.RandomNoiseFactor = EditorGUILayout.FloatField("Random noise factor", meshGenerator.RandomNoiseFactor);
            }
            else
            {
                meshGenerator.TerrainSize = EditorGUILayout.IntField("Size", meshGenerator.TerrainSize);
            }
        }

        using (var group = new EditorGUILayout.FadeGroupScope(System.Convert.ToSingle(sinControls)))
        {
            if (sinControls)
            {
                meshGenerator.RandomNoiseFactor = EditorGUILayout.FloatField("Random noise factor", meshGenerator.RandomNoiseFactor);
                meshGenerator.SinHeightLimit = EditorGUILayout.FloatField("Height limit", meshGenerator.SinHeightLimit);
                meshGenerator.SinMultiplier = EditorGUILayout.FloatField("Multiplier", meshGenerator.SinMultiplier);
            }
            else
            {
                meshGenerator.HeightMin = EditorGUILayout.FloatField("Height min", meshGenerator.HeightMin);
                meshGenerator.HeightMax = EditorGUILayout.FloatField("Height max", meshGenerator.HeightMax);
            }
        }

        meshGenerator.Scale = EditorGUILayout.FloatField("Scale", meshGenerator.Scale);
        
        meshGenerator.TerrainMaterial = (Material)EditorGUILayout.ObjectField("Terrain material", meshGenerator.TerrainMaterial, typeof(Material));

        EditorGUILayout.Space();
    }
}
