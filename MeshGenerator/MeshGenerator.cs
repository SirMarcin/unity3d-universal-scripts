﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGenerator : MonoBehaviour
{
    public static Mesh GenerateMesh(float [,] heightMap, int size, float scale)
    {
        Mesh mesh = new Mesh();

        Vector3[] v = new Vector3[size * size];

        int trianglesCount = ((size - 1) * (size - 1)) * 6;
        int[] triangles = new int[trianglesCount];

        int[,] matrix = new int[size, size];

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                if (i == 0)
                {
                    matrix[i, j] = j;
                }
                else if (i % 2 == 0)
                {
                    matrix[i, j] = matrix[i - 1, j] + 2 * j + 1;
                }
                else
                {
                    matrix[i, j] = matrix[i - 1, j] + (2 * size - 1) - 2 * j;
                }

                v[matrix[i, j]] = new Vector3(i * scale, heightMap[i, j], j * scale);
            }
        }

        triangles = GenerateTriangles(matrix, trianglesCount);

        mesh.Clear();
        mesh.vertices = v;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();

        return mesh;
    }

    private static int[] GenerateTriangles(int[,] matrix, int trianglesCount)
    {
        int matrixSize = matrix.GetLength(0);
        int[] triangles = new int[trianglesCount];
        int index = 0;


        for (int i = 0; i < matrixSize - 1; i++)
        {
            for (int j = 0; j < matrixSize - 1; j++)
            {
                triangles[index++] = matrix[i, j];
                triangles[index++] = matrix[i, j + 1];
                triangles[index++] = matrix[i + 1, j + 1];

                triangles[index++] = matrix[i, j];
                triangles[index++] = matrix[i + 1, j + 1];
                triangles[index++] = matrix[i + 1, j];
            }
        }

        return triangles;
    }
}
