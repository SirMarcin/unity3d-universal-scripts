﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class TerrainGridGenerator : MonoBehaviour
{
    public enum Size
    {
        _3x3 = 3,
        _5x5 = 5,
        _9x9 = 9,
        _17x17 = 17,
        _33x33 = 33,
        _65x65 = 65
    }

    public enum GenerationAlgorithm
    {
        SimpleRandom = 0,
        Sinusoidal = 1,
        DiamondSquare = 2
    }

    public GenerationAlgorithm TerrainGenerationAlgorithm;
    public int TerrainSize = (int)Size._9x9;
    public float Scale = 5f;
    public float HeightMin = -5f;
    public float HeightMax = 5f;
    public float RandomNoiseFactor = 1f;
    public float SinHeightLimit = 5f;
    public float SinMultiplier = 1f;
    public Material TerrainMaterial;

    [Header("Preview")]
    public bool GenerateVerticePreview = false;
    public GameObject VerticePreviewObj;

    private float[,] matrix;
    private List<GameObject> pointsList;
    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;
    private Vector3[] vertices;
    private Mesh mesh;

    void Start()
    {
        if (VerticePreviewObj == null)
        {
            GenerateVerticePreview = false;
        }

        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        meshRenderer.material = TerrainMaterial;

        meshFilter = gameObject.GetComponent<MeshFilter>();

        GenerateTerrainGrid();
    }

    public void GenerateTerrainGrid()
    {
        DestroyCurrentTerrain();
        ReInitTerrainValues();

        for (int i = 0; i < matrix.GetLength(0); i++)
        {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                if (GenerateVerticePreview)
                {
                    pointsList.Add(GameObject.Instantiate(VerticePreviewObj, new Vector3(i * Scale, matrix[i, j], j * Scale), transform.rotation));
                }
            }
        }

        meshFilter.mesh = MeshGenerator.GenerateMesh(matrix, TerrainSize, Scale);

        gameObject.AddComponent<MeshCollider>();
    }

    private void DestroyCurrentTerrain()
    {
        if (pointsList == null)
        {
            return;
        }

        var c = gameObject.GetComponent<MeshCollider>();

        if (c != null)
        {
            Destroy(c);
        }

        foreach (var obj in pointsList)
        {
            Destroy(obj);
        }
    }


    private void ReInitTerrainValues()
    {
        vertices = new Vector3[TerrainSize * TerrainSize];
        pointsList = new List<GameObject>();

        switch (TerrainGenerationAlgorithm)
        {
            case GenerationAlgorithm.SimpleRandom:
                matrix = Matrix.GetSquareMatrixRandom(TerrainSize, HeightMin, HeightMax);
                break;
            case GenerationAlgorithm.Sinusoidal:
                matrix = Matrix.GetSquareMatrixSin(TerrainSize, SinHeightLimit, SinMultiplier, RandomNoiseFactor);
                break;
            case GenerationAlgorithm.DiamondSquare:
                matrix = Matrix.GetDiamondSquareMatrix(TerrainSize,
                    Random.Range(HeightMin, HeightMax),
                    Random.Range(HeightMin, HeightMax),
                    Random.Range(HeightMin, HeightMax),
                    Random.Range(HeightMin, HeightMax),
                    RandomNoiseFactor);
                break;
            default:
                break;
        }

        int vIndex = 0;

        for (int i = 0; i < matrix.GetLength(0); i++)
        {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                vertices[vIndex++] = new Vector3(i * Scale, matrix[i, j], j * Scale);
            }
        }
    }
}
