﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matrix
{
    public static float[,] GetSquareMatrixRandom(int size, float min, float max)
    {
        float[,] matrix = new float[size, size];

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                matrix[i, j] = Random.Range(min, max);
            }
        }

        return matrix;
    }

    public static float[,] GetSquareMatrixSin(int size, float heightRange, float multiplier, float randomNoiseFactor)
    {
        float[,] matrix = new float[size, size];
        float step;
        multiplier = multiplier * 2;

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                matrix[i, j] = (Mathf.Sin(multiplier * Mathf.PI * ((float)j / (float)size)) + Mathf.Sin(multiplier * Mathf.PI * ((float)i / (float)size))) * heightRange + Random.Range(-randomNoiseFactor, randomNoiseFactor);
            }
        }

        return matrix;
    }

    public static float[,] GetDiamondSquareMatrix(int size, float seed1, float seed2, float seed3, float seed4, float randomFactor)
    {
        bool[,] matrix = new bool[size, size];
        float[,] matrixToReturn = new float[size, size];
        int currentX = 0, currentY = 0;
        float step;

        matrixToReturn[0, 0] = seed1;
        matrixToReturn[0, size - 1] = seed2;
        matrixToReturn[size - 1, size - 1] = seed3;
        matrixToReturn[size - 1, 0] = seed4;
        
        int x1 = 0;
        int x2 = size - 1;
        int y1 = 0;
        int y2 = size - 1;

        currentX = currentY = (size + 1) / 2 - 1;

        step = currentX * 2;

        while (step >= 1f)
        {
            int initialY = currentY;
            
            while (currentX < size)
            {
                currentY = initialY;

                x1 = currentX - (int)Mathf.Ceil(step / 2);
                x2 = currentX + (int)Mathf.Ceil(step / 2);

                if (x1 < 0)
                {
                    x1 = size - 1;
                }

                if (x2 > size - 1)
                {
                    x2 = 0;
                }

                while (currentY <= size - 1)
                {
                    if (matrix[currentX, currentY] == false)
                    {
                        y1 = currentY - (int)(step / 2);
                        y2 = currentY + (int)(step / 2);

                        if (y1 < 0)
                        {
                            y1 = size - 1;
                        }

                        if (y2 > size - 1)
                        {
                            y2 = 0;
                        }

                        matrixToReturn[currentX, currentY] = (matrixToReturn[x1, y1] + matrixToReturn[x1, y2] + matrixToReturn[x2, y1] + matrixToReturn[x2, y2]) / 4 + Random.Range(-randomFactor, randomFactor);
                        matrix[currentX, currentY] = true;
                    }

                    currentY += (int)step;
                }

                currentX += (int)step;
            }

            step = step / 2;

            if (step == 1)
            {
                currentX = 0;
                currentY = 0;
            }
            else
            {
                currentX = (int)(step / 2);
                currentY = (int)(step / 2);
            }

            randomFactor = randomFactor * .7f;
        }

        return matrixToReturn;
    }
}
