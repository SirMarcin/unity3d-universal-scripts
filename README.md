Universal Unity Scripts

== MeshGenerator ==


INSTRUCTIONS: Import the ProceduralTerrain.unitypackage file into Unity and read following descriptions. 'Prefabs' folder contains suggested use cases.

TerrainGridGenerator.cs - when attached to a GameObject it automatically generates square terrain based on provided parameters (generation takes place in Start() method).


== UiAnimator ==

INSTRUCTIONS: Import the GuiObjectAnimation.unitypackage file into Unity and read following descriptions. 'Prefabs' folder contains suggested use cases.

GuiObjectAnimation.cs - attach this script to UI GameObject that's on Canvas and set parameters accordingly to make the object animate. Animation is based on movement across X and Y axis, it's modulated by sin() function.
