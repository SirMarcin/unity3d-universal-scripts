﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiObjectAnimation : MonoBehaviour
{
    public bool Animate = true;
    
    [Header("Settings")]
    public Vector2 MovementMaxValues;
    public float MovementSpeed = 2f;

    [Header("Behaviour")]
    public bool Invert;
    public bool Bounce;

    public bool StoppingAnimation { get; private set; }

    private int dir;
    private RectTransform rect;
    private Vector2 currentPosition;
    private Vector2 originalPosition;
    private float sinModulated;

    void Start()
    {
        dir = Invert ? -1 : 1;

        rect = gameObject.GetComponent<RectTransform>();

        currentPosition = originalPosition = rect.anchoredPosition;
        StoppingAnimation = false;
    }

    void Update()
    {
        if (!Animate)
        {
            return;
        }

        sinModulated = Mathf.Sin(Time.timeSinceLevelLoad * MovementSpeed);

        if (Bounce)
        {
            currentPosition.x = Mathf.Abs(sinModulated * MovementMaxValues.x) * dir + originalPosition.x;
            currentPosition.y = Mathf.Abs(sinModulated * MovementMaxValues.y) * dir + originalPosition.y;
        }
        else
        {
            currentPosition.x = sinModulated * MovementMaxValues.x * dir + originalPosition.x;
            currentPosition.y = sinModulated * MovementMaxValues.y * dir + originalPosition.y;
        }

        rect.anchoredPosition = currentPosition;
    }

    public void StopAnimationOnBottom()
    {
        StartCoroutine(StopAnimationOnValue(0.1f));
    }

    public void StopAnimationOnTop()
    {
        StartCoroutine(StopAnimationOnValue(MovementMaxValues.y));
    }

    private IEnumerator StopAnimationOnValue(float value)
    {
        StoppingAnimation = true;

        while (Animate)
        {
            if (Mathf.Abs(sinModulated) <= value)
            {
                Animate = false;
            }

            yield return new WaitForEndOfFrame();
        }
    }

}
